#ifndef __MODBUS_READ_WRITE_H__
#define __MODBUS_READ_WRITE_H__

#include "mb.h"
#include "mbport.h"

void free_modbus_init(eMBMode eMode, UCHAR ucSlaveAddress, UCHAR ucPort, ULONG ulBaudRate, eMBParity eParity);

#endif
