#ifndef __UART_H__
#define __UART_H__

#define  UART5_RX_ENABLE       0
#define  UART5_RX_DISABLE      1

#define  UART5_TX_ENABLE       0
#define  UART5_TX_DISABLE      1

extern uint8_t uart5_rx_state;//0-enable,  1-disable
extern uint8_t uart5_tx_state;//0-enable,  1-disable
extern uint8_t uart5_recv_data;             //uart5接收buffer
//初始化uart4
void uart4_init(void);
//初始化uart5
void uart5_init(void);
#endif
