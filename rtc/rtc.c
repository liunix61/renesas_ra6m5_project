#include "hal_data.h"
#include <stdio.h>
#include <time.h>

#define YEARS_SINCE_1900    (2022 - 1900)

uint8_t rtc_time_int_flag = 0;

/* rtc_time_t is an alias for the C Standard time.h struct 'tm' */
rtc_time_t set_time =
{
    .tm_sec  = 0,
    .tm_min  = 12,
    .tm_hour = 22,
    .tm_mday = 29,
    //.tm_wday = 2 - 1,
    .tm_mon  = 11 - 1,
    .tm_year = YEARS_SINCE_1900,
};
rtc_time_t get_time;

/*
** rtc初始化
*/
void rtc_init (void)
{
   fsp_err_t err = FSP_SUCCESS;
   /* Open the RTC module */
   err = R_RTC_Open(&g_rtc0_ctrl, &g_rtc0_cfg);
   if(err != FSP_SUCCESS)
   {
      printf("rtc open error\r\n");
   }	
   /* Set the calendar time */
   //R_RTC_CalendarTimeSet(&g_rtc0_ctrl, &set_time);//第一次设置时间需要调用
   /* Get the calendar time */
   err = R_RTC_CalendarTimeGet(&g_rtc0_ctrl, &get_time);
   if(err != FSP_SUCCESS)
   {
      printf("rtc CalendarTimeGet error\r\n");
   }				
   printf("get time: %d-%02d-%02d  %02d:%02d:%02d\r\n", get_time.tm_year + 1900, get_time.tm_mon + 1,
               get_time.tm_mday, get_time.tm_hour, get_time.tm_min, get_time.tm_sec);
   /* Set the calendar time */
   R_RTC_CalendarTimeSet(&g_rtc0_ctrl, &get_time);    
   /* Set the periodic interrupt rate to 1 second */
   R_RTC_PeriodicIrqRateSet(&g_rtc0_ctrl, RTC_PERIODIC_IRQ_SELECT_1_SECOND);	
}

/*
** 打印rtc时间，每秒一次
*/
void rtc_time_print(void)
{
   //判断是否需要打印时间
   if(rtc_time_int_flag != 0)
   {
      rtc_time_int_flag = 0;
      R_BSP_SoftwareDelay (20, BSP_DELAY_UNITS_MILLISECONDS);
      //注意，R_RTC_CalendarTimeGet()函数不能在中断函数（包括回调函数）里调用，否则会出现莫名其妙的错误
      //这一点，fsp手册里也写的很清楚
      R_RTC_CalendarTimeGet(&g_rtc0_ctrl, &get_time);
      printf("time: %d-%02d-%02d  %02d:%02d:%02d\r\n", get_time.tm_year + 1900, get_time.tm_mon + 1,
            get_time.tm_mday, get_time.tm_hour, get_time.tm_min, get_time.tm_sec);      
   }
}

/*
** rtc中断回调函数，每秒中断一次 
*/
void rtc0_callback(rtc_callback_args_t * p_args)
{
	  if(RTC_EVENT_PERIODIC_IRQ == p_args->event)
		{
			rtc_time_int_flag = 1;//指示需要进行时间显示
		}
    
}

