#include "hal_data.h"
#include "uart.h"
#include <stdio.h>
#include "modbus_read_write.h"
#include "can.h"
#include "rtc.h"

extern uint8_t rtc_time_int_flag;
extern rtc_time_t get_time;
FSP_CPP_HEADER
void R_BSP_WarmStart(bsp_warm_start_event_t event);
FSP_CPP_FOOTER

/*******************************************************************************************************************//**
 * main() is generated by the RA Configuration editor and is used to generate threads if an RTOS is used.  This function
 * is called by main() when no RTOS is used.
 **********************************************************************************************************************/
void hal_entry(void)
{
    /* TODO: add your own code here */
    /* 初始化GPIO端口 */
    R_IOPORT_Open(&g_ioport_ctrl, &g_bsp_pin_cfg);
    /* 初始化uart4，作为调试串口使用 */
    uart4_init();
    uart5_init();//初始化uart5
    printf("Renesas RA6M5 Start...\r\n");
    /* 初始化Modbus端口和状态 */
    free_modbus_init(MB_RTU, 1, 5, 9600, MB_PAR_NONE);
    //R_BSP_SoftwareDelay (500, BSP_DELAY_UNITS_MILLISECONDS);
    /* CAN接口初始化 */
    canfd_init();
	rtc_init();
    while(1)
    {
        /* 状态机轮询 */
        ( void )eMBPoll();		
        //rtc_time_print();
        //判断是否需要打印时间
        if(rtc_time_int_flag != 0)
        {
            rtc_time_int_flag = 0;
            //注意，R_RTC_CalendarTimeGet()函数不能在中断函数（包括回调函数）里调用，否则会出现莫名其妙的错误
            //这一点，fsp手册里也写的很清楚
            R_RTC_CalendarTimeGet(&g_rtc0_ctrl, &get_time);
            printf("time: %d-%02d-%02d  %02d:%02d:%02d\r\n", get_time.tm_year + 1900, get_time.tm_mon + 1,
                    get_time.tm_mday, get_time.tm_hour, get_time.tm_min, get_time.tm_sec);      
        }        
    }
#if BSP_TZ_SECURE_BUILD
    /* Enter non-secure code */
    R_BSP_NonSecureEnter();
#endif
}

/*******************************************************************************************************************//**
 * This function is called at various points during the startup process.  This implementation uses the event that is
 * called right before main() to set up the pins.
 *
 * @param[in]  event    Where at in the start up process the code is currently at
 **********************************************************************************************************************/
void R_BSP_WarmStart (bsp_warm_start_event_t event)
{
    if (BSP_WARM_START_RESET == event)
    {
#if BSP_FEATURE_FLASH_LP_VERSION != 0

        /* Enable reading from data flash. */
        R_FACI_LP->DFLCTL = 1U;

        /* Would normally have to wait tDSTOP(6us) for data flash recovery. Placing the enable here, before clock and
         * C runtime initialization, should negate the need for a delay since the initialization will typically take more than 6us. */
#endif
    }

    if (BSP_WARM_START_POST_C == event)
    {
        /* C runtime environment and system clocks are setup. */

        /* Configure pins. */
        R_IOPORT_Open(&g_ioport_ctrl, g_ioport.p_cfg);
    }
}

#if BSP_TZ_SECURE_BUILD

BSP_CMSE_NONSECURE_ENTRY void template_nonsecure_callable ();

/* Trustzone Secure Projects require at least one nonsecure callable function in order to build (Remove this if it is not required to build). */
BSP_CMSE_NONSECURE_ENTRY void template_nonsecure_callable ()
{

}
#endif
