/*
 * FreeModbus Libary: BARE Port
 * Copyright (C) 2006 Christian Walter <wolti@sil.at>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * File: $Id$
 */

/* ----------------------- Platform includes --------------------------------*/
#include "port.h"

/* ----------------------- Modbus includes ----------------------------------*/
#include "mb.h"
#include "mbport.h"
#include "hal_data.h"
#include <stdio.h>
/* ----------------------- static functions ---------------------------------*/
static void prvvTIMERExpiredISR( void );

/* ----------------------- Start implementation -----------------------------*/
BOOL
xMBPortTimersInit( USHORT usTim1Timerout50us )
{
    /* Initializes the module. */
    fsp_err_t err = R_GPT_Open(&g_timer3_ctrl, &g_timer3_cfg);
    if(err != FSP_SUCCESS)
    {
        printf("gpt3 open error\r\n");
    }  

    /* 设置timer的中断周期
    ** 100表示timer的计数时钟是100MHz
     */
    err = R_GPT_PeriodSet(&g_timer3_ctrl, (uint32_t)usTim1Timerout50us * 50U * 100U);
    if(err != FSP_SUCCESS)
    {
        printf("gpt3 periodset error\r\n");
    }  
    return TRUE;
}


void
vMBPortTimersEnable(  )
{
    /* Enable the timer with the timeout passed to xMBPortTimersInit( ) */
    /* Read the current counter value. Counter value is in status.counter. */
    timer_status_t status;
    fsp_err_t err = R_GPT_StatusGet(&g_timer3_ctrl, &status);
    if(err != FSP_SUCCESS)
    {
        printf("gpt3 get status error\r\n");
    }  

    if(status.state == TIMER_STATE_COUNTING)
    {
        /* Disable any pending timers. */
        fsp_err_t err = R_GPT_Stop (&g_timer3_ctrl);
        if(err != FSP_SUCCESS)
        {
            printf("gpt3 stop error\r\n");
        }          
    }

    err = R_GPT_Reset (&g_timer3_ctrl);
    if(err != FSP_SUCCESS)
    {
        printf("gpt3 reset error\r\n");
    }    

    /* Start the timer. */
    err = R_GPT_Start(&g_timer3_ctrl);
    if(err != FSP_SUCCESS)
    {
        printf("gpt3 start error\r\n");
    }      
}

void
vMBPortTimersDisable(  )
{
    /* Read the current counter value. Counter value is in status.counter. */
    timer_status_t status;
    fsp_err_t err = R_GPT_StatusGet(&g_timer3_ctrl, &status);
    if(err != FSP_SUCCESS)
    {
        printf("gpt3 get status error\r\n");
    }  

    if(status.state == TIMER_STATE_COUNTING)
    {
        /* Disable any pending timers. */
        fsp_err_t err = R_GPT_Stop (&g_timer3_ctrl);
        if(err != FSP_SUCCESS)
        {
            printf("gpt3 stop error\r\n");
        }          
    }      
}

/* Create an ISR which is called whenever the timer has expired. This function
 * must then call pxMBPortCBTimerExpired( ) to notify the protocol stack that
 * the timer has expired.
 */
static void prvvTIMERExpiredISR( void )
{
    ( void )pxMBPortCBTimerExpired(  );
}


/* 定时器中断回调函数. */
void g_timer3_callback (timer_callback_args_t * p_args)
{
    uint8_t led_status;
    switch(p_args->event)
    {
        case TIMER_EVENT_CYCLE_END:
               
            // R_IOPORT_PinRead (&g_ioport_ctrl, BSP_IO_PORT_04_PIN_00, (bsp_io_level_t *)&led_status);
            // if(led_status)
            // {
            //     R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_04_PIN_00, BSP_IO_LEVEL_LOW);
            // }
            // else
            // {
            //     R_IOPORT_PinWrite(&g_ioport_ctrl, BSP_IO_PORT_04_PIN_00, BSP_IO_LEVEL_HIGH);
            // }
            /* Add application code to be called periodically here. */
            prvvTIMERExpiredISR();
        break;

        default:

        break;
    }
}
