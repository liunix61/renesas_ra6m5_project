#include "hal_data.h"
#include <stdio.h>

canfd_afl_entry_t p_canfd0_afl[CANFD_CFG_AFL_CH0_RULE_NUM] = 
{
    /* Store all data frames with at least 4 bytes from Standard IDs 0x40-0x4F in RX FIFO 0 and RX FIFO 1 */
    {
        .id =
        {
            .id         = 0x40,
            .frame_type = CAN_FRAME_TYPE_DATA,
            .id_mode    = CAN_ID_MODE_STANDARD
        },
        .mask =
        {
            .mask_id         = 0x7F0,
            .mask_frame_type = 1,
            .mask_id_mode    = 1
        },
        .destination =
        {
            .minimum_dlc       = CANFD_MINIMUM_DLC_4,
            .rx_buffer         = CANFD_RX_MB_NONE,
            .fifo_select_flags = (canfd_rx_fifo_t) (CANFD_RX_FIFO_0)
        }
    }    
};


#define CAN_EXAMPLE_ID    (0x20)
can_frame_t g_can_tx_frame;
can_frame_t g_can_rx_frame;
volatile canfd_error_t g_err_status = (canfd_error_t) 0;

/**
 * can回调函数
*/
void canfd0_callback (can_callback_args_t * p_args)
{
    switch (p_args->event)
    {
        case CAN_EVENT_RX_COMPLETE:    /* Receive complete event. */
        {
            /* Read received frame */
            memcpy(&g_can_rx_frame, &p_args->frame, sizeof(can_frame_t));
			printf("id : 0x%x\r\n",g_can_rx_frame.id);
            printf("data : ");
            for(uint8_t i = 0; i < g_can_rx_frame.data_length_code; i++)
            {
                printf("0x%x ", g_can_rx_frame.data[i]);
            }
            printf("\r\n");
            /* Handle event */
            break;
        }
        case CAN_EVENT_TX_COMPLETE:    /* Transmit complete event. */
        {
            /* Handle event */
            break;
        }
        case CAN_EVENT_ERR_GLOBAL:                        /* Global error. */
        case CAN_EVENT_ERR_CHANNEL:                       /* Channel error. */
        {
            /* Get error status */
            g_err_status = (canfd_error_t) p_args->error; /* Check error code with canfd_error_t. */
            /* Handle event */
            break;
        }
        default:
        {
            break;
        }
    }
}

/**
 * can初始化
*/
void canfd_init (void)
{
    fsp_err_t err;
    /* Initialize the CAN module */
    err = R_CANFD_Open(&g_canfd0_ctrl, &g_canfd0_cfg);
    /* Handle any errors. This function should be defined by the user. */
    assert(FSP_SUCCESS == err);
    /* Setup frame to write to CAN ID 0x20 */
    g_can_tx_frame.id               = CAN_EXAMPLE_ID;
    g_can_tx_frame.id_mode          = CAN_ID_MODE_STANDARD;
    g_can_tx_frame.type             = CAN_FRAME_TYPE_DATA;
    g_can_tx_frame.data_length_code = 8;
    g_can_tx_frame.options          = 0;
    /* Write some data to the transmit frame */
    for (uint32_t i = 0; i < 8; i++)
    {
        g_can_tx_frame.data[i] = (uint8_t) i;
    }
    /* Send data on the bus */
    err = R_CANFD_Write(&g_canfd0_ctrl, CANFD_TX_MB_0, &g_can_tx_frame);
    assert(FSP_SUCCESS == err);
    /* Wait for a transmit callback event */
}
